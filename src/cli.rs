use std::ffi::OsString;

use structopt::StructOpt;

#[derive(StructOpt)]
#[structopt(name="sq-keyring-linter",
            set_term_width=72,
            about="\n\
\"sq-keyring-linter\" checks for and optionally repairs OpenPGP
certificates that use SHA-1.",

            after_help="\
\"sq-keyring-linter\" checks the supplied certificates for the following
SHA-1-related issues:

  - Whether a certificate revocation uses SHA-1.

  - Whether the current self signature for a non-revoked User ID uses
    SHA-1.

  - Whether the current subkey binding signature for a non-revoked,
    live subkey uses SHA-1.

  - Whether a primary key binding signature (a \"backsig\") for a
    non-revoked, live subkey uses SHA-1.

Diagnostics are printed to stderr.  At the end, some statistics are
shown.  This is useful when examining a keyring.  If \"--fix\" is
specified and at least one issue could be fixed, the fixed
certificates are printed to stdout.

This tool does not currently support smart cards.  But, if only the
subkeys are on a smart card, this tool may still be able to partially
repair the certificate.  In particular, it will be able to fix any
issues with User ID self signatures and subkey binding signatures for
encryption-capable subkeys, but it will not be able to generate new
primary key binding signatures for any signing-capable subkeys.

EXIT STATUS:

If \"--fix\" is not specified:
  2  if any issues were found,
  1  if not issues were found, but there were errors reading the input,
  0  if there were no issues.

If \"--fix\" is specified:
  3  if any issues could not be fixed,
  1  if not issues were found, but there were errors reading the input,
  0  if all issues were fixed or there were no issues.

EXAMPLES:

  # To gather statistics, simply run:
  $ sq-keyring-linter keyring.pgp

  # To fix a key:
  $ gpg --export-secret-keys FPR | sq-keyring-linter --fix -p passw0rd -p password123 | gpg --import

  # To get a list of keys with issues:
  $ sq-keyring-linter --list-keys keyring.pgp | while read FPR; do something; done

SEE ALSO:

sq-keyring-linter's homepage: <https://gitlab.com/sequoia-pgp/keyring-linter>
")]
pub struct Linter {
    // (progn (make-local-variable 'fill-column) (setq fill-column 70))

    /// Quiet; does not output any diagnostics.
    #[structopt(short, long)]
    pub quiet: bool,

    /// Attempts to fix certificates, when possible.
    #[structopt(short, long)]
    pub fix: bool,

    /// When fixing a certificate, the fixed certificate is exported
    /// without any secret key material.  Using this switch causes any
    /// secret key material to also be exported.
    #[structopt(short, long, verbatim_doc_comment)]
    pub export_secret_keys: bool,

    /// A key's password.  Normally this is not needed: if stdin is
    /// connected to a tty, the linter will ask for a password when
    /// needed.
    #[structopt(
        short,
        long,
        number_of_values = 1,
        multiple = true,
        verbatim_doc_comment
    )]
    pub password: Vec<String>,

    /// If set, outputs a list of fingerprints, one per line, of
    /// certificates that have issues.  This output is intended for
    /// use by scripts.
    ///
    /// This option implies "--quiet". If you also specify "--fix",
    /// errors will still be printed to stderr, and fixed certificates
    /// will still be emitted to stdout.
    #[structopt(short="k", long, verbatim_doc_comment)]
    pub list_keys: bool,

    /// A list of OpenPGP keyrings to process.  If none are specified,
    /// a keyring is read from stdin.
    #[structopt(parse(from_os_str), value_names = &["FILES"], verbatim_doc_comment)]
    pub inputs: Vec<OsString>,
}
